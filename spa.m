function varargout = spa(net, Y, P)
  for i = 1:size(net,3)

    A = net(:,:,i);
    A2 = A;
    A2(find(eye(size(A,1)))) = -1;
    [U,S,V] = svd(A2);
    S(S<1/max(abs(Y(:))))=0;
    A3 = U*S*V';
    Yest = -pinv(A3)*P;

    %% Standard normalize both Y and Yest to have mean=0 and stdev=1
    YN = (Y - mean(Y(:)))/std(Y(:));
    YestN = (Yest - mean(Yest(:)))/std(Yest(:));

    % now calculating the badness of fit from their absolute values
    rms(i) = norm(abs(YestN) - abs(YN), 'fro');

    nrlinks(i) = nnz(A)/size(A,1);
    K(i) = nnz(sum(logical(A)));

  end

  rms = exp((rms-min(rms))/(max(rms)-min(rms)));
  rms = (rms-min(rms))/(max(rms)-min(rms));
  K = (K-min(K))/(max(K)-min(K));

  gic_penalty = K;
  GIC = K+rms;

  [~,gin] = min(GIC);

  varargout{1} = net(:,:,gin);
  varargout{2} = gic_penalty;
  varargout{3} = nrlinks;
  varargout{4} = rms;
  varargout{5} = GIC;

end
